/*
Conteo de incidentes y Total de Horas de Soporte seg�n Clasificaci�n 
Listado de incidentes cuyo Estado sea Pendiente
Conteo de incidentes por mes seg�n Perfil y Prioridad.
Conteo de incidentes por Perfil cuya cantidad sea mayor a 10.
*/


Create view VistaIncidentesXClasif as
  select NomClasificacion, count(1) Incidentes, sum(HorasSoporte) HorasSoporte
  from Clasificacion c, Ticket t
  where c.codClasificacion = t.codClasificacion
  Group by NomClasificacion
  Order by NomClasificacion


Create view VistaIncidentesPendientes as
  select perf.nomPerfil, u.nombre, u.apellido, p.codPrioridad, p.Nomprioridad, t.asunto, t.fecha, c.nomClasificacion
  from Ticket T, Clasificacion c, Prioridad p, Usuario u, Perfil perf
  where t.codClasificacion = c.codClasificacion
  and t.CodPrioridad = p.codPrioridad
  and t.codUsuario = u.codUsuario
  and u.codPerfil = perf.codPerfil
  and t.codEstado = 2  -- 2 es el codigo para estado Pendiente
  order by Fecha -- el mas antiguo de primero...
  

Create view VistaIncidentesXMes as
  select p.nomPerfil, prio.nomPrioridad, to_char(t.fecha,'mm') mes, count(1) TotalIncidentes, sum(HorasSoporte) TotalHorasSoporte
  from ticket t, Prioridad prio, Usuario u, Perfil p
  where t.codPrioridad = prio.codPrioridad
  and t.codUsuario = u.codUsuario
  and u.codPerfil = p.codPerfil
  group by p.nomPerfil, prio.nomPrioridad, to_char(t.fecha,'mm')
  order by p.nomPerfil, prio.nomPrioridad, to_char(t.fecha,'mm')
 
 
  
Create view VistaIncidentesXPerfil as
  select p.nomPerfil, count(1) TotalIncidentes, sum(HorasSoporte) TotalHorasSoporte
  from ticket t, Usuario u, Perfil p
  where t.codUsuario = u.codUsuario
  and u.codPerfil = p.codPerfil
  group by p.nomPerfil
  having count(1)> 10
  order by p.nomPerfil
 
