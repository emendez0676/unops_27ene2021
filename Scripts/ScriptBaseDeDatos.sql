
CREATE TABLE PERFIL
(	
  CodPerfil NUMBER NOT NULL ENABLE, 
	NomPerfil VARCHAR2(50),
  PRIMARY KEY (CodPerfil)
);


CREATE TABLE USUARIO
(	
  CodUsuario NUMBER NOT NULL ENABLE, 
	Nombre VARCHAR2(50),
  Apellido VARCHAR2(50),
  CodPerfil NUMBER,
  Primary key (CodUsuario),
  Foreign key (CodPerfil) references perfil(codPerfil)
);


CREATE TABLE CLASIFICACION
(	
  CodClasificacion NUMBER NOT NULL ENABLE, 
	NomClasificacion VARCHAR2(50),
  PRIMARY KEY (CodClasificacion)
);


CREATE TABLE ESTADO
(	
  CodEstado NUMBER NOT NULL ENABLE, 
	NomEstado VARCHAR2(50),
  PRIMARY KEY (CodEstado)
);


CREATE TABLE PRIORIDAD
(	
  CodPrioridad NUMBER NOT NULL ENABLE, 
	NomPrioridad VARCHAR2(50),
  PRIMARY KEY (CodPrioridad)
);


CREATE TABLE TICKET
(	
  CodTicket NUMBER NOT NULL ENABLE, 
	Asunto VARCHAR2(100),
  Descripcion VARCHAR2(1000),
  Fecha DATE,
  HorasSoporte NUMBER,
  CodUsuario NUMBER,
  CodClasificacion NUMBER,
  CodPrioridad NUMBER,
  CodEstado NUMBER,
  Primary key (CodTicket),
  Foreign key (CodUsuario) references Usuario(codUsuario),
  Foreign key (CodClasificacion) references Clasificacion(codClasificacion),
  Foreign key (CodPrioridad) references Prioridad(codPrioridad),
  Foreign key (CodEstado) references Estado(codEstado)
);



